package main

import (
	"bytes"
	"fmt"
	"log"
	"regexp"
	"strings"
	"sync"

	"github.com/cheggaaa/pb/v3"
	"github.com/yterajima/go-sitemap"
	"gopkg.in/headzoo/surf.v1"
)

// Defines metadata about a site, and contains a collection of webpages
type Site struct {
	webpages   []Webpage
	baseURL    string
	siteMapURL string
}

// Defines metadata about a webpage, and contains a collection of images
type Webpage struct {
	url    string
	images []Image
}

// An image simply consists of a name and content
type Image struct {
	name    string
	content []byte
}

// Populates webpages for a given site.
func (site *Site) getSiteURLs() {
	smap, err := sitemap.Get(site.siteMapURL, nil)
	if err != nil {
		log.Printf("Unable to get sitemap from %s: %v", site.siteMapURL, err)
	}

	var pages []Webpage
	for _, URL := range smap.URL {
		pages = append(pages, Webpage{url: URL.Loc})
	}

	site.webpages = pages
}

// Crawls all Webpages for a given Site
func (site *Site) crawlSiteForImages() {

	var wg sync.WaitGroup

	bar := pb.New(len(site.webpages))
	bar.Set("prefix", "Fetching from all sitemap URLs: ")
	bar.SetTemplateString(string(pb.Simple))
	bar.SetMaxWidth(120)
	if err := bar.Err(); err != nil {
		log.Printf("Error setting up progress bar: %v", err)
	}
	bar.Start()

	for i := range site.webpages {
		wg.Add(1)
		go site.webpages[i].crawlWebpageForImages(&wg, bar)
	}

	wg.Wait()
	bar.Finish()
}

// Crawl all Images on a given Webpage
func (webpage *Webpage) crawlWebpageForImages(wg *sync.WaitGroup, bar *pb.ProgressBar) {

	defer wg.Done()

	bow := surf.NewBrowser()

	err := bow.Open(webpage.url)
	if err != nil {
		log.Fatalf("Error opening webpage: %v", err)
	}

	for _, image := range bow.Images() {
		var buf bytes.Buffer
		_, err := image.Download(&buf)
		if err != nil {
			log.Fatalf("Error downloading image: %v", err)
		}

		DLimage := Image{
			name:    image.Title,
			content: buf.Bytes(),
		}
		webpage.images = append(webpage.images, DLimage)
	}
	bar.Increment()
}

// Crawl will perform a crawl for images. It will return a site object
func (site *Site) crawl() {
	site.getSiteURLs() // Populates site with all of the webpages from the sitemap
	site.crawlSiteForImages()
}

// Saves the given site to ./output
func (site *Site) save() {

	// Make a Regex to say we only want letters and numbers
	reg, err := regexp.Compile("[^a-zA-Z0-9-]+")
	if err != nil {
		log.Fatal(err)
	}
	formattedSite := reg.ReplaceAllString(site.baseURL, "_")
	outdir := fmt.Sprintf("./output/%v", formattedSite)

	mkdir("./output")
	mkdir(outdir)
	for _, page := range site.webpages {

		processedUrl := reg.ReplaceAllString(page.url, "_")
		processedUrl = strings.Replace(processedUrl, formattedSite, "", 1)
		processedUrl = strings.TrimLeft(processedUrl, "_")
		mkdir(fmt.Sprintf("%s/%s", outdir, processedUrl))

		for _, image := range page.images {
			if len(image.name) > 0 {
				write(fmt.Sprintf("%s/%s/%s", outdir, processedUrl, image.name), image.content)
			}
		}
	}
}
