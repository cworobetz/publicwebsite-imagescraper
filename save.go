package main

import (
	"log"
	"os"
)

// Makes a directory at the specified path.
func mkdir(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.Mkdir(path, 0755)
		if err != nil {
			log.Fatalf("Unable to make output directory: %v", err)
		}
	}
}

// Writes a file, given a path and content
func write(path string, content []byte) {
	f, err := os.Create(path)

	if err != nil {
		log.Fatalf("Error creating path: %v", err)
	}

	defer f.Close()

	_, err = f.Write(content)

	if err != nil {
		log.Fatalf("Error writing to file: %v", err)
	}
}
