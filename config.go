package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	site Site
}

func parseConfig() Config {

	// Load env vars from .env file
	err := godotenv.Load()
	if err != nil {
		log.Printf("Environment file not found, continuing.")
	}

	baseURL := lookupEnv("URL")
	siteMapURL := fmt.Sprintf("%s/sitemap.xml", baseURL) // Inferred from baseURL

	// Build the config object
	config := Config{
		Site{
			baseURL:    baseURL,
			siteMapURL: siteMapURL,
		},
	}

	return config
}

// Looks up an environment variable and returns its value. Logs a fatal error if the environment variable is not set
func lookupEnv(key string) string {
	val, present := os.LookupEnv(key)
	if !present {
		log.Fatalf("Error configuring vault: %s environment variable is not set", key)
	}
	return val
}
