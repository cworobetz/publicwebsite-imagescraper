module pwsscraper

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.6.1 // indirect
	github.com/cheggaaa/pb/v3 v3.0.8
	github.com/headzoo/surf v1.0.0 // indirect
	github.com/headzoo/ut v0.0.0-20181013193318-a13b5a7a02ca // indirect
	github.com/joho/godotenv v1.3.0
	github.com/yterajima/go-sitemap v0.2.2
	gopkg.in/headzoo/surf.v1 v1.0.0
)
