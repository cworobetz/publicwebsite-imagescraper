package main

import (
	"log"
	"time"
)

func main() {
	start := time.Now()

	config := parseConfig() // Grab the config. Currently, only the URL we're scraping

	log.Printf("Starting crawl of %s", config.site.baseURL)

	config.site.crawl() // Crawl all of the webpages and download their images
	config.site.save()  // Save all the images it found

	elapsed := time.Since(start)
	log.Printf("Took %s", elapsed)
}
