# pwsscraper

pwsscraper is a tool used to scrape images from a public website. It works by scraping the given sites' `sitemap.xml` for URLs, then it will individually visit each page and save all of its images. It outputs to a folder under `./output`.

## Configuration

Clone the repository to your local computer. Rename `.env.example` to `.env`, and fill in the values as desired. You can also simply use environment variables, without editing the file - they will take precedence over the file.

## Usage

First, if you don't have `go` installed and wish to be provided a compiled binary to run, please ask me and I will send you the binary and further instructions.

Run the following command in the folder, ensuring either the filled-in `.env` file is present, or the env variables have been set.

```bash
go run .
```

## Internals

This program has concurrency built in. This means the program will finish scraping a public website very quickly, with some medium sized sites taking only 20-30 seconds on a wireless connection with a laptop.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
